import 'package:renon/database/task.dart';

int calculateTaskPercentage(List<Task> tasks) {
  if (tasks.length > 0) {
    var completdTasks = tasks.where((task) => task.completed == true);
    int percentage = ((completdTasks.length / tasks.length) * 100).round();
    return percentage;
  }
  return 0;
}

Duration getDateDiffDuration(DateTime previousDate, DateTime nextDate) {
  if (previousDate == null || nextDate == null) {
    return Duration();
  }
  final differenceInSeconds = nextDate.difference(previousDate).inSeconds;

  if (differenceInSeconds > 0) {
    return Duration(seconds: differenceInSeconds);
  } else {
    return Duration(seconds: 0);
  }
}
