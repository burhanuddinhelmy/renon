import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:renon/provider/goal_task.provider.dart';
import 'package:renon/provider/user.provider.dart';
import 'package:renon/view/splash/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => GoalAndTaskProvider()),
      ],
      child: MaterialApp(
          darkTheme: ThemeData(
            fontFamily: 'montserrat',

            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: MaterialColor(0xFF004774, {
              0: Color(0xFFFFFFFF),
              50: Color(0xFFE0E9EE),
              100: Color(0xFFB3C8D5),
              200: Color(0xFF80A3BA),
              300: Color(0xFF4D7E9E),
              400: Color(0xFF266389),
              500: Color(0xFF004774),
              600: Color(0xFF00406C),
              700: Color(0xFF003761),
              800: Color(0xFF002F57),
              900: Color(0xFF002044)
            }),
            primaryColor: Color(0xFF004774),
            brightness: Brightness.dark,
            accentColor: Color(0xFFED500D),
            primaryColorLight: Color(0xFF4D7E9E),
            cardTheme: CardTheme(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20))),
            buttonTheme: ButtonThemeData(
                height: 50,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.red))),
            // This makes the visual density adapt to the platform that you run
            // the app on. For desktop platforms, the controls will be smaller and
            // closer together (more dense) than on mobile platforms.
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          navigatorKey: Get.key,
          title: 'Flutter Demo',
          theme: ThemeData(
            fontFamily: 'montserrat',

            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: MaterialColor(0xFF004774, {
              0: Color(0xFFFFFFFF),
              50: Color(0xFFE0E9EE),
              100: Color(0xFFB3C8D5),
              200: Color(0xFF80A3BA),
              300: Color(0xFF4D7E9E),
              400: Color(0xFF266389),
              500: Color(0xFF004774),
              600: Color(0xFF00406C),
              700: Color(0xFF003761),
              800: Color(0xFF002F57),
              900: Color(0xFF002044)
            }),
            brightness: Brightness.light,
            accentColor: Color(0xFFED500D),
            primaryColorLight: Color(0xFF4D7E9E),
            cardTheme: CardTheme(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20))),
            buttonTheme: ButtonThemeData(
                height: 50,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.red))),
            // This makes the visual density adapt to the platform that you run
            // the app on. For desktop platforms, the controls will be smaller and
            // closer together (more dense) than on mobile platforms.
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: Splash()),
    );
  }
}
