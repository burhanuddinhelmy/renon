import 'package:renon/database/database.dart';
import 'package:sqflite/sqflite.dart';

final String table = 'user';
final String columnId = '_id';
final String columnName = 'name';
final String columnCompletedGoals = 'completedGoals';
final String columnCompletedTasks = 'completedTasks';
final String columnCharacterId = 'characterId';

final String columnTitle = 'title';
final String columnCompleted = 'completed';
final String columnEndDate = 'endDate';
final String columnReward = 'reward';

class User {
  int id;
  String name;
  int completedTasks;
  int completedGoals;
  int characterId;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnName: name,
      columnCompletedGoals: completedGoals,
      columnCompletedTasks: completedTasks,
      columnCharacterId: characterId,
    };

    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  User();

  User.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    name = map[columnName];
    completedGoals = map[columnCompletedGoals];
    completedTasks = map[columnCompletedTasks];
    characterId = map[columnCharacterId];
  }
}

class UserDB {
  static final UserDB _userDB = UserDB._internal();
  factory UserDB() {
    return _userDB;
  }
  UserDB._internal();

  String get create {
    return '''CREATE TABLE $table (
        $columnId INTEGER PRIMARY KEY,
        $columnName TEXT NOT NULL,
        $columnCompletedGoals INTEGER NOT NULL,
        $columnCompletedTasks INTEGER NOT NULL,
        $columnCharacterId INTEGER NOT NULL
        )''';
  }

  Future<User> insert(User todo) async {
    todo.id = await DB().db.insert(table, todo.toMap());
    return todo;
  }

  Future<User> getUser() async {
    List<Map> maps = await DB().db.query(
          table,
        );
    if (maps.length > 0) {
      return User.fromMap(maps.first);
    }
    return null;
  }

  Future<int> delete(int id) async {
    return await DB().db.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> update(User todo) async {
    var insert = await DB().db.update(table, todo.toMap(),
        where: '$columnId = ?', whereArgs: [todo.id]);
    return insert;
  }

  Future close() async => DB().db.close();
}
