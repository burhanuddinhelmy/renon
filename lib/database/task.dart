import 'package:renon/database/database.dart';
import 'package:sqflite/sqflite.dart';

final String table = 'task';
final String columnId = '_id';
final String columnGoalId = 'goalId';
final String columnTitle = 'title';
final String columnNote = 'note';
final String columnCompleted = 'completed';

class Task {
  int id;
  int goalId;
  String title;
  bool completed;
  String note;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnTitle: title,
      columnGoalId: goalId,
      columnNote: note,
      columnCompleted: completed ? 1 : 0,
    };

    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  Task();

  Task.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    title = map[columnTitle];
    goalId = map[columnGoalId];
    note = map[columnNote];
    completed = map[columnCompleted] == 1 ? true : false;
  }
}

class TaskDB {
  static final TaskDB _taskDB = TaskDB._internal();

  factory TaskDB() {
    return _taskDB;
  }
  TaskDB._internal();

  String get create {
    return '''CREATE TABLE $table (
        $columnId INTEGER PRIMARY KEY,
        $columnGoalId INTEGER NOT NULL,
        $columnTitle TEXT,
        $columnNote TEXT,
        $columnCompleted NTEGER NOT NULL
        )''';
  }

  Future<int> insert(Task task) async {
    return DB().db.insert(table, task.toMap());
  }

  Future<Task> getTask() async {
    List<Map> maps = await DB().db.query(
          table,
        );
    if (maps.length > 0) {
      return Task.fromMap(maps.first);
    }
    return null;
  }

  Future<int> getCompletedTaskCount() async {
    List<Map> maps = await DB().db.rawQuery('''
    SELECT COUNT(*)
    FROM $table
    WHERE $columnCompleted = 1;
    ''');
    return maps.first['COUNT(*)'];
  }

  Future<List<Task>> getTaskList(int goalId) async {
    List<Task> tasks = [];
    List<Map> maps = await DB()
        .db
        .query(table, where: '$columnGoalId = ?', whereArgs: [goalId]);

    tasks = maps.map((map) => Task.fromMap(map)).toList();

    return tasks;
  }

  Future<int> delete(int id) {
    return DB().db.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> deleteByGoalId(int id) {
    return DB().db.delete(table, where: '$columnGoalId = ?', whereArgs: [id]);
  }

  Future<int> update(Task todo) async {
    var insert = await DB().db.update(table, todo.toMap(),
        where: '$columnId = ?', whereArgs: [todo.id]);
    return insert;
  }

  Future close() async => DB().db.close();
}
