import 'package:renon/database/database.dart';
import 'package:renon/database/task.dart';
import 'package:sqflite/sqflite.dart';

final String table = 'goal';
final String columnId = '_id';
final String columnTitle = 'title';
final String columnCompleted = 'completed';
final String columnEndDate = 'endDate';
final String columnReward = 'reward';

class Goal {
  int id;
  String title;
  bool completed;
  DateTime endDate;
  String reward;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnTitle: title,
      columnCompleted: completed ? 1 : 0,
      columnEndDate: endDate != null ? endDate.toIso8601String() : null,
      columnReward: reward,
    };

    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  Goal();

  Goal.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    title = map[columnTitle];
    completed = map[columnCompleted] == 1 ? true : false;
    endDate = map[columnEndDate] != null
        ? DateTime.tryParse(map[columnEndDate])
        : null;
    reward = map[columnReward];
  }
}

class GoalAndTasks {
  Goal goal;
  List<Task> tasks;

  GoalAndTasks({this.goal, this.tasks});
}

class GoalDB {
  static final GoalDB _goalDB = GoalDB._internal();
  factory GoalDB() {
    return _goalDB;
  }
  GoalDB._internal();

  String get create {
    return '''CREATE TABLE goal (
        $columnId INTEGER PRIMARY KEY,
        $columnTitle TEXT,
        $columnCompleted INTEGER NOT NULL,
        $columnEndDate TEXT,
        $columnReward TEXT
        )''';
  }

  Future<Goal> insert(Goal todo) async {
    await DB().db.insert(table, todo.toMap());
    return todo;
  }

  Future<List<GoalAndTasks>> getGoalAndTasksList() async {
    List<GoalAndTasks> goalsAndTasks = [];
    List<Map> goalMaps = await DB().db.query(
          table,
          orderBy: "_id DESC",
        );
    if (goalMaps.length > 0) {
      var futures = goalMaps.map((goalMap) async {
        return GoalAndTasks(
            goal: Goal.fromMap(goalMap),
            tasks: await TaskDB().getTaskList(goalMap[columnId]));
      }).toList();
      goalsAndTasks = await Future.wait(futures);
    }
    return goalsAndTasks;
  }

  Future<GoalAndTasks> getGoalWithTasks(int id) async {
    List<Map> goalMaps = await DB().db.query(
      table,
      where: '$columnId = ?',
      whereArgs: [id],
    );

    List<Map> taskMaps = await DB().db.query(
      'task',
      where: 'goalId = ?',
      whereArgs: [goalMaps.first[columnId]],
    );

    return GoalAndTasks(
        goal: Goal.fromMap(goalMaps.first),
        tasks: taskMaps.map((taskMap) => Task.fromMap(taskMap)).toList());
  }

  Future<dynamic> getGoal(id) async {
    List<Map> maps = await DB().db.query(
      table,
      where: '$columnId = ?',
      whereArgs: [id],
    );
    if (maps.length > 0) {
      return Goal.fromMap(maps.first);
    }
    return null;
  }

  Future<int> delete(int id) async {
    return await DB().db.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> update(Goal todo) async {
    var insert = await DB().db.update(table, todo.toMap(),
        where: '$columnId = ?', whereArgs: [todo.id]);
    return insert;
  }

  Future<int> getCompletedGoalCount() async {
    List<Map> maps = await DB().db.rawQuery('''
    SELECT COUNT(*)
    FROM $table
    WHERE $columnCompleted = 1;
    ''');
    return maps.first['COUNT(*)'];
  }

  Future close() async => DB().db.close();
}
