import 'package:renon/database/goal.dart';
import 'package:renon/database/task.dart';
import 'package:renon/database/user.dart';
import 'package:sqflite/sqflite.dart';

class DB {
  static final DB _database = DB._internal();
  Database db;
  factory DB() {
    return _database;
  }
  DB._internal();

  Future<void> init() async {
    db = await openDatabase("root", version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(GoalDB().create);
      await db.execute(UserDB().create);
      await db.execute(TaskDB().create);
    });

    return;
  }

  Future close() async => db.close();
}
