import 'package:flame/flame.dart';
import 'package:flame/animation.dart' as animation;
import 'package:flame/sprite.dart';
import 'package:flame/spritesheet.dart';
import 'package:flame/widgets/animation_widget.dart';
import 'package:flame/widgets/sprite_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:renon/provider/user.provider.dart';

// ignore: must_be_immutable
class CharacterAnimated extends StatefulWidget {
  bool noAnimtaion;

  CharacterAnimated({this.noAnimtaion = true});
  @override
  _CharacterAnimatedState createState() => _CharacterAnimatedState();
}

class _CharacterAnimatedState extends State<CharacterAnimated> {
  bool _loaded = false;
  Sprite _sprite;
  animation.Animation _animation;

  loadData() async {
    String url =
        "animation/idle/${context.read<UserProvider>().user.characterId}.png";
    _sprite = await Sprite.loadSprite(url, width: 32, height: 30);

    await Flame.images.load(url);
    final _animationSpriteSheet = SpriteSheet(
      imageName: url,
      columns: 4,
      rows: 1,
      textureWidth: 32,
      textureHeight: 30,
    );
    _animation = _animationSpriteSheet.createAnimation(
      0,
      stepTime: 0.5,
      to: 4,
    );

    setState(() {
      _loaded = true;
    });
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return _loaded
        ? widget.noAnimtaion
            ? SpriteWidget(sprite: _sprite)
            : AnimationWidget(animation: _animation)
        : CircularProgressIndicator();
  }
}
