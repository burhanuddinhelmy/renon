import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:renon/database/user.dart';
import 'package:renon/provider/user.provider.dart';

// ignore: must_be_immutable
class Character extends StatefulWidget {
  Character();
  @override
  _CharacterState createState() => _CharacterState();
}

class _CharacterState extends State<Character> {
  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.fill,
      child: Image(
          alignment: Alignment.center,
          image: AssetImage(
              'assets/image/monster/${context.watch<UserProvider>().user.characterId}.png')),
    );
  }
}
