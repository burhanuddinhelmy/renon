import 'package:flutter/material.dart';

class RadioButton extends StatelessWidget {
  bool isChecked;
  Function onChange;
  RadioButton({this.isChecked = false, this.onChange(bool isChecked)});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {onChange(this.isChecked)},
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Theme.of(context).backgroundColor),
            child: Padding(
                padding: const EdgeInsets.all(7.0),
                child: Icon(
                  Icons.close,
                  size: 20.0,
                  color: Theme.of(context).primaryColorLight,
                )),
          ),
          Container(
            padding: EdgeInsets.all(isChecked ? 6.0 : 0),
            decoration: BoxDecoration(
                shape: BoxShape.circle, color: Theme.of(context).primaryColor),
            child: isChecked
                ? Icon(
                    Icons.check,
                    size: 15.0,
                    color: Colors.white,
                  )
                : Container(),
          ),
        ],
      ),
    );
  }
}
