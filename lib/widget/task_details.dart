import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:renon/database/goal.dart';
import 'package:renon/database/task.dart';
import 'package:renon/provider/goal_task.provider.dart';
import 'package:renon/util/util.dart';
import 'package:renon/widget/radio_button.dart';
// ignore: must_be_immutable

class TaskDetails extends StatefulWidget {
  bool editTitle = false;
  bool isNew;

  TaskDetails({this.isNew = false});

  @override
  _TaskDetailsState createState() => _TaskDetailsState();
}

class _TaskDetailsState extends State<TaskDetails> {
  @override
  Widget build(BuildContext context) {
    GoalAndTasks goalAndTasks =
        context.watch<GoalAndTaskProvider>().goalAndTasks;
    return Container(
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 80),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: TasksProgress(goalAndTasks: goalAndTasks),
          )
        ],
      ),
    );
  }
}

class TaskItem extends StatefulWidget {
  // bool editTitle = false;
  Task task;

  TaskItem({@required this.task});

  @override
  _TaskItemState createState() => _TaskItemState();
}

class _TaskItemState extends State<TaskItem> {
  String _title = '';
  String _note = '';

  @override
  void initState() {
    _title = widget.task.title;
    _note = widget.task.note;

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      direction: DismissDirection.endToStart,
      onDismissed: (_) {
        context.read<GoalAndTaskProvider>().deleteTask(widget.task);
        context
            .read<GoalAndTaskProvider>()
            .updateSelectGoal(widget.task.goalId);
      },
      key: Key(widget.task.id.toString()),
      background: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            IconButton(
              onPressed: () {},
              color: Colors.white,
              icon: Icon(Icons.delete_outline),
            )
          ],
        ),
        color: Colors.red,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: RadioButton(
                  isChecked: widget.task.completed,
                  onChange: (completed) {
                    widget.task.completed = !completed;
                    context.read<GoalAndTaskProvider>().updateTask(widget.task);
                    return;
                  }),
            ),
            Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextField(
                        scrollPadding: EdgeInsets.all(0),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            decoration: widget.task.completed
                                ? TextDecoration.lineThrough
                                : null,
                            decorationColor: Theme.of(context).accentColor),
                        controller: TextEditingController(text: _title),
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            border: InputBorder.none,
                            hintText: 'Task description',
                            alignLabelWithHint: true,
                            hintStyle: TextStyle(color: Colors.black12)),
                        onChanged: (text) {
                          _title = text;
                        },
                        onEditingComplete: () {
                          widget.task.title = _title;
                          widget.task.note = _note;
                          context
                              .read<GoalAndTaskProvider>()
                              .updateTask(widget.task);
                          FocusScope.of(context).unfocus();
                        }),
                    TextField(
                        scrollPadding: EdgeInsets.all(0),
                        style: TextStyle(
                            fontSize:
                                Theme.of(context).textTheme.caption.fontSize,
                            decoration: widget.task.completed
                                ? TextDecoration.lineThrough
                                : null,
                            decorationColor: Theme.of(context).accentColor),
                        controller: TextEditingController(text: _note),
                        decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.symmetric(vertical: 2),
                            border: InputBorder.none,
                            hintText: 'Notes',
                            alignLabelWithHint: true,
                            hintStyle: TextStyle(color: Colors.black12)),
                        onChanged: (text) {
                          _note = text;
                        },
                        onEditingComplete: () {
                          widget.task.title = _title;
                          widget.task.note = _note;
                          context
                              .read<GoalAndTaskProvider>()
                              .updateTask(widget.task);
                          FocusScope.of(context).unfocus();
                        })
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}

class TasksProgress extends StatefulWidget {
  GoalAndTasks goalAndTasks;

  TasksProgress({@required this.goalAndTasks});
  @override
  _TasksProgressState createState() => _TasksProgressState();
}

class _TasksProgressState extends State<TasksProgress> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("${widget.goalAndTasks.tasks.length} Tasks"),
                  Text(
                      "${calculateTaskPercentage(widget.goalAndTasks.tasks)} %")
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Stack(
            children: [
              Container(
                height: 10,
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColorLight,
                    borderRadius: BorderRadius.circular(10)),
              ),
              AnimatedContainer(
                curve: Curves.easeInCubic,
                height: 10,
                width: calculateTaskPercentage(widget.goalAndTasks.tasks) /
                    100 *
                    MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: calculateTaskPercentage(widget.goalAndTasks.tasks) ==
                            100
                        ? Theme.of(context).accentColor
                        : Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(10)),
                duration: Duration(seconds: 1),
              )
            ],
          ),
        ),
        ...widget.goalAndTasks.tasks
            .asMap()
            .entries
            .map((entry) => TaskItem(task: entry.value))
            .toList()
      ],
    );
  }
}

class NewTaskButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: EdgeInsets.only(left: 32, right: 16),
      color: Theme.of(context).primaryColor,
      shape: new RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(30.0),
      ),
      child: Row(
        children: [
          Text(
            "ADD TASK",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          Icon(
            Icons.add,
            color: Colors.white,
          )
        ],
      ),
      onPressed: () {
        GoalAndTasks goalAndTasks =
            context.read<GoalAndTaskProvider>().goalAndTasks;
        Task newTask = Task()
          ..goalId = goalAndTasks.goal.id
          ..completed = false
          ..title = "New task";
        context.read<GoalAndTaskProvider>().createTask(newTask);
      },
    );
  }
}
