import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:renon/database/goal.dart';
import 'package:renon/database/task.dart';
import 'package:renon/provider/goal_task.provider.dart';
import 'package:renon/util/util.dart';
import 'package:renon/widget/radio_button.dart';
import 'package:renon/widget/countdown_pill.dart';
// ignore: must_be_immutable

// ignore: must_be_immutable
class TaskCardDetails extends StatefulWidget {
  GoalAndTasks goalAndTasks;
  TaskCardDetails(this.goalAndTasks);

  @override
  _TaskCardDetailsState createState() => _TaskCardDetailsState();
}

class _TaskCardDetailsState extends State<TaskCardDetails> {
  ConfettiController _controllerCenter;

  int get _completedTaskCount {
    return widget.goalAndTasks.tasks.where((task) => !task.completed).length;
  }

  @override
  void initState() {
    _controllerCenter =
        ConfettiController(duration: const Duration(seconds: 1));
    super.initState();
  }

  @override
  void dispose() {
    _controllerCenter.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.goalAndTasks.tasks.length != 0 && _completedTaskCount == 0) {
      _controllerCenter.play();
    }
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Wrap(
            children: [
              CountdownPill(widget.goalAndTasks.goal),
              Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.goalAndTasks.goal.title,
                          style: TextStyle(
                              fontSize: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .fontSize,
                              fontWeight: FontWeight.bold),
                        ),
                        widget.goalAndTasks.tasks.length != 0
                            ? _completedTaskCount == 0
                                ? Text(
                                    "Congratulation! you can enjoy your reward now.",
                                  )
                                : Text(
                                    "Complete $_completedTaskCount more task and you can enjoy your reward",
                                  )
                            : Text(
                                "Start to add task now",
                              ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: [
                        ConfettiWidget(
                          confettiController: _controllerCenter,
                          blastDirectionality: BlastDirectionality
                              .explosive, // don't specify a direction, blast randomly
                          shouldLoop:
                              false, // start again as soon as the animation is finished
                          colors: const [
                            Colors.green,
                            Colors.blue,
                            Colors.pink,
                            Colors.orange,
                            Colors.purple
                          ], // manually specify the colors to be used
                        ),
                        CircleAvatar(
                          radius: 40,
                          backgroundColor: Theme.of(context).primaryColorLight,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              widget.goalAndTasks.goal.reward,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Theme.of(context).primaryColorDark),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: TasksProgress(this.widget.goalAndTasks),
              )
            ],
          ),
        ),
        Column(
          children: [
            Spacer(),
            Container(
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: new LinearGradient(
                    colors: [
                      Theme.of(context).cardColor.withOpacity(0),
                      Theme.of(context).cardColor,
                    ],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(0.0, 1.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp),
              ),
              height: 100,
            ),
          ],
        ),
      ],
    );
  }
}

// ignore: must_be_immutable
class TaskItem extends StatelessWidget {
  Task task;

  TaskItem({@required this.task});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: RadioButton(
                isChecked: task.completed,
                onChange: (completed) {
                  task.completed = !completed;
                  context.read<GoalAndTaskProvider>().updateTask(task);
                  context
                      .read<GoalAndTaskProvider>()
                      .checkGoalIsComplete(task.goalId);
                  return;
                }),
          ),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              task.title != null ? task.title : "New task",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  decoration:
                      task.completed ? TextDecoration.lineThrough : null,
                  decorationColor: Theme.of(context).accentColor,
                  decorationThickness: 2),
            ),
            Text(
              task.note != null ? task.note : "Task note",
              style: TextStyle(
                  color: task.note != null
                      ? Theme.of(context).textTheme.bodyText1.color
                      : Theme.of(context)
                          .textTheme
                          .bodyText1
                          .color
                          .withOpacity(0.5),
                  fontSize: Theme.of(context).textTheme.caption.fontSize,
                  decoration:
                      task.completed ? TextDecoration.lineThrough : null,
                  decorationColor: Theme.of(context).accentColor,
                  decorationThickness: 2),
            ),
          ])
        ],
      ),
    );
  }
}

class TasksProgress extends StatefulWidget {
  GoalAndTasks goalAndTasks;

  TasksProgress(this.goalAndTasks);
  @override
  _TasksProgressState createState() => _TasksProgressState();
}

class _TasksProgressState extends State<TasksProgress> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("${widget.goalAndTasks.tasks.length} Tasks"),
                  Text(
                      "${calculateTaskPercentage(widget.goalAndTasks.tasks)} %")
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Stack(
            children: [
              Container(
                height: 10,
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColorLight,
                    borderRadius: BorderRadius.circular(10)),
              ),
              AnimatedContainer(
                curve: Curves.easeInCubic,
                height: 10,
                width: calculateTaskPercentage(widget.goalAndTasks.tasks) /
                    100 *
                    MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: calculateTaskPercentage(widget.goalAndTasks.tasks) ==
                            100
                        ? Theme.of(context).accentColor
                        : Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(10)),
                duration: Duration(seconds: 1),
              )
            ],
          ),
        ),
        ...widget.goalAndTasks.tasks
            .asMap()
            .entries
            .map((entry) => TaskItem(task: entry.value))
            .toList()
      ],
    );
  }
}
