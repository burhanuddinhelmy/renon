import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:renon/database/goal.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:renon/provider/goal_task.provider.dart';
import 'package:renon/util/util.dart';
import 'package:slide_countdown_clock/slide_countdown_clock.dart';

class CountdownPill extends StatefulWidget {
  Goal goal;
  CountdownPill(this.goal);

  @override
  _CountdownPillState createState() => _CountdownPillState();
}

class _CountdownPillState extends State<CountdownPill> {
  Duration duration;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    duration = getDateDiffDuration(DateTime.now(), widget.goal.endDate);
    Color textColor = this.widget.goal.endDate != null
        ? Colors.white
        : Theme.of(context).accentColor;
    return Row(
      children: [
        Stack(
          alignment: AlignmentDirectional.center,
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: this.widget.goal.endDate != null
                      ? Theme.of(context).accentColor
                      : Colors.white,
                  border: Border.all(color: Theme.of(context).accentColor)),
              padding: EdgeInsets.symmetric(vertical: 2, horizontal: 6),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: Icon(
                      this.widget.goal.endDate != null
                          ? Icons.timer
                          : Icons.add,
                      color: textColor,
                      size: Theme.of(context).textTheme.caption.fontSize,
                    ),
                  ),
                  this.widget.goal.endDate != null
                      ? duration.compareTo(Duration(seconds: 0)) != 0
                          ? SlideCountdownClock(
                              duration: duration,
                              slideDirection: SlideDirection.Up,
                              separator: ": ",
                              textStyle: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .fontSize,
                                color: textColor,
                              ),
                              separatorTextStyle: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .fontSize,
                                fontWeight: FontWeight.bold,
                                color: textColor,
                              ),
                              onDone: () {},
                            )
                          : Text("Time up",
                              style: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .fontSize,
                                color: textColor,
                              ))
                      : Text("Add Countdown",
                          style: TextStyle(
                            fontSize:
                                Theme.of(context).textTheme.caption.fontSize,
                            color: textColor,
                          ))
                ],
              ),
            ),
            Opacity(
              opacity: 0,
              child: Container(
                color: Colors.red,
                constraints: BoxConstraints(maxWidth: 80, maxHeight: 15),
                child: DateTimePicker(
                  style: TextStyle(
                      fontSize: Theme.of(context).textTheme.caption.fontSize),
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.symmetric(vertical: 2),
                      border: InputBorder.none,
                      hintText: 'Notes',
                      alignLabelWithHint: true,
                      hintStyle: TextStyle(color: Colors.black12)),
                  // initialValue:
                  firstDate: DateTime(2000),
                  dateMask: 'd MMM, yyyy',
                  initialValue: DateTime.now().toString(),
                  lastDate: DateTime(2100),
                  dateLabelText: 'Date',
                  use24HourFormat: false,
                  type: DateTimePickerType.dateTime,
                  onChanged: (val) {
                    this.widget.goal.endDate = DateTime.tryParse(val);
                    context
                        .read<GoalAndTaskProvider>()
                        .updateGoal(this.widget.goal);
                    context
                        .read<GoalAndTaskProvider>()
                        .updateSelectGoal(this.widget.goal.id);
                  },
                  validator: (val) {
                    return null;
                  },
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
