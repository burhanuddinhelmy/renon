import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:renon/database/goal.dart';
import 'package:renon/provider/goal_task.provider.dart';
import 'package:renon/widget/character_anim.dart';
import 'package:renon/widget/task_details.dart';
import 'package:renon/widget/countdown_pill.dart';

class ViewOrEditGoal extends StatefulWidget {
  @override
  _ViewOrEditGoalState createState() => _ViewOrEditGoalState();
}

class _ViewOrEditGoalState extends State<ViewOrEditGoal> {
  String _title = '';
  String _reward = '';
  GoalAndTasks _goalAndTasks;
  ConfettiController _controllerCenter;
  final FocusNode focusNode = FocusNode();

  int get _completedTaskCount {
    return _goalAndTasks.tasks.where((task) => !task.completed).length;
  }

  @override
  void initState() {
    _controllerCenter =
        ConfettiController(duration: const Duration(seconds: 1));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _goalAndTasks = context.watch<GoalAndTaskProvider>().goalAndTasks;
    _title = context.watch<GoalAndTaskProvider>().goalAndTasks.goal.title;
    _reward = context.watch<GoalAndTaskProvider>().goalAndTasks.goal.reward;

    if (_goalAndTasks.tasks.length != 0 && _completedTaskCount == 0) {
      _controllerCenter.play();
    }

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            NewTaskButton(),
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: CustomScrollView(
          slivers: [
            SliverAppBar(
              bottom: PreferredSize(
                  child: Stack(
                    alignment: Alignment.bottomLeft,
                    children: [
                      Container(
                        height: 100,
                        child: LayoutBuilder(
                          builder: (BuildContext context,
                              BoxConstraints constraints) {
                            return Container(
                                width: constraints.minHeight,
                                height: constraints.minWidth,
                                child: Transform.scale(
                                  scale: 1.1,
                                  child: CharacterAnimated(
                                    noAnimtaion: false,
                                  ),
                                ));
                          },
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ConfettiWidget(
                            confettiController: _controllerCenter,
                            blastDirectionality: BlastDirectionality
                                .explosive, // don't specify a direction, blast randomly
                            shouldLoop:
                                false, // start again as soon as the animation is finished
                            colors: const [
                              Colors.green,
                              Colors.blue,
                              Colors.pink,
                              Colors.orange,
                              Colors.purple
                            ], // manually specify the colors to be used
                          ),
                          Text(
                            'Your reward',
                            style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .color
                                    .withOpacity(0.3)),
                          ),
                          TextField(
                            scrollPadding: EdgeInsets.all(0),
                            textAlign: TextAlign.center,
                            controller: TextEditingController(
                                text: context
                                    .watch<GoalAndTaskProvider>()
                                    .goalAndTasks
                                    .goal
                                    .reward),
                            onChanged: (text) {
                              _reward = text;
                            },
                            onEditingComplete: () {
                              Goal goal = context
                                  .read<GoalAndTaskProvider>()
                                  .goalAndTasks
                                  .goal;
                              goal.reward = _reward;
                              context
                                  .read<GoalAndTaskProvider>()
                                  .updateGoal(goal);
                              FocusScope.of(context).unfocus();
                            },
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .fontSize,
                              fontWeight: FontWeight.bold,
                            ),
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding:
                                    EdgeInsets.symmetric(vertical: 2),
                                border: InputBorder.none,
                                hintText: 'Your reward',
                                alignLabelWithHint: true,
                                hintStyle: TextStyle(
                                    color: Colors.white.withOpacity(0.3))),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CountdownPill(context
                                    .watch<GoalAndTaskProvider>()
                                    .goalAndTasks
                                    .goal),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  preferredSize: Size(MediaQuery.of(context).size.width, 150)),
              title: TextField(
                scrollPadding: EdgeInsets.all(0),
                textAlign: TextAlign.center,
                controller: TextEditingController(
                    text: context
                        .watch<GoalAndTaskProvider>()
                        .goalAndTasks
                        .goal
                        .title),
                onChanged: (text) {
                  _title = text;
                },
                onEditingComplete: () {
                  Goal goal =
                      context.read<GoalAndTaskProvider>().goalAndTasks.goal;
                  goal.title = _title;
                  context.read<GoalAndTaskProvider>().updateGoal(goal);
                  FocusScope.of(context).unfocus();
                },
                style: TextStyle(
                  color: Colors.white,
                  fontSize: Theme.of(context).textTheme.headline6.fontSize,
                  fontWeight: FontWeight.bold,
                ),
                focusNode: focusNode,
                decoration: InputDecoration(
                    isDense: true,
                    contentPadding: EdgeInsets.symmetric(vertical: 2),
                    border: InputBorder.none,
                    hintText: 'Goal title',
                    alignLabelWithHint: true,
                    hintStyle: TextStyle(color: Colors.white.withOpacity(0.3))),
              ),
              actions: [
                IconButton(
                    icon: Icon(Icons.delete_outline),
                    onPressed: () async {
                      await context.read<GoalAndTaskProvider>().deleteGoal(
                          context.read<GoalAndTaskProvider>().goalAndTasks);
                      Get.back();
                    })
              ],
              iconTheme: IconThemeData(color: Colors.white),
              backgroundColor: Theme.of(context).primaryColorLight,
              elevation: 0,
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: TaskDetails(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
