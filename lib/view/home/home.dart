import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:renon/database/goal.dart';
import 'package:renon/provider/goal_task.provider.dart';
import 'package:renon/view/update_account.dart';
import 'package:renon/widget/character.dart';
import 'package:renon/provider/user.provider.dart';
import 'package:renon/view/home/goal.dart';
import 'package:renon/widget/character_anim.dart';
import 'package:renon/widget/task_card_details.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ScrollController bottomSheetScroll;
  int _activePageIndex = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      context.read<UserProvider>().getUser();
      context.read<GoalAndTaskProvider>().getGoalAndTasksList();
      context.read<GoalAndTaskProvider>().getFinishedTaskAndGoalCount();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(40),
                  bottomLeft: Radius.circular(40)),
              color: Theme.of(context).primaryColor,
            ),
            height: context.mediaQuery.size.height / 2,
          ),
          Positioned(
            left: -15.0,
            top: context.mediaQuery.size.height / 6,
            child: Container(
              height: context.mediaQuery.size.height / 6,
              width: context.mediaQuery.size.height / 6,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Theme.of(context).accentColor,
              ),
            ),
          ),
          Positioned(
            right: -context.mediaQuery.size.width / 8,
            top: -context.mediaQuery.size.height / 20,
            child: Container(
              height: context.mediaQuery.size.height / 3.5,
              width: context.mediaQuery.size.height / 3.5,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Theme.of(context).accentColor,
              ),
            ),
          ),
          SafeArea(
              child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  children: [
                    HeaderSection(),
                    CharacterAndQuoteSection(),
                  ],
                ),
              ),
              Expanded(
                  child: Column(
                children: [
                  Expanded(child: TaskCardSection((index) {
                    setState(() {
                      _activePageIndex = index;
                    });
                    return;
                  })),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 32, vertical: 10),
                    child: Row(
                      children: [
                        ...context
                            .watch<GoalAndTaskProvider>()
                            .goalAndTasksList
                            .asMap()
                            .entries
                            .map((entry) {
                          return SliderIndicator(
                              active: _activePageIndex == entry.key);
                        }).toList()
                      ],
                    ),
                  )
                ],
              )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  NewGoalsButton(),
                ],
              )
            ],
          )),
        ],
      ),
    );
  }
}

class HeaderSection extends StatelessWidget {
  const HeaderSection({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var user = context.watch<UserProvider>().user;
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () async {
                await Get.to(AccountSettings());
                context.read<UserProvider>().getUser();
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Icon(
                      Icons.account_circle_outlined,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Welcome back",
                        style: TextStyle(
                            fontSize:
                                Theme.of(context).textTheme.caption.fontSize,
                            color: Colors.white),
                      ),
                      Container(
                          width: context.mediaQuery.size.width / 3,
                          child: Text(
                            user.name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          )),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Spacer(),
          Column(
            children: [
              Text(
                context.watch<GoalAndTaskProvider>().finishTaskCount.toString(),
                style: TextStyle(
                    fontSize: Theme.of(context).textTheme.headline6.fontSize,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              Container(
                  width: 80,
                  child: Text(
                    "Finished Task",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: Theme.of(context).textTheme.caption.fontSize,
                        color: Colors.white),
                  )),
            ],
          ),
          Column(
            children: [
              Text(
                context.watch<GoalAndTaskProvider>().finishGoalCount.toString(),
                style: TextStyle(
                    fontSize: Theme.of(context).textTheme.headline6.fontSize,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              Container(
                  width: 80,
                  child: Text(
                    "Finished Goal",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: Theme.of(context).textTheme.caption.fontSize,
                        color: Colors.white),
                  )),
            ],
          ),
        ],
      ),
    );
  }
}

class CharacterAndQuoteSection extends StatelessWidget {
  const CharacterAndQuoteSection({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                return Container(
                    width: constraints.minHeight,
                    height: constraints.minWidth,
                    child: CharacterAnimated(
                      noAnimtaion: false,
                    ));
              },
            ),
          ),
          Expanded(
            flex: 5,
            child: Row(
              children: [
                Triangle(),
                Expanded(
                  child: Card(
                    margin: EdgeInsets.all(0),
                    child: Container(
                      padding: EdgeInsets.all(
                        20,
                      ),
                      child: Column(
                        children: [
                          Text(
                            "Push yourself, because no one else is going to do it for you.",
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text(
                                  "Quote by Anonymous",
                                  style: TextStyle(
                                      fontSize: Theme.of(context)
                                          .textTheme
                                          .caption
                                          .fontSize,
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class TaskCardSection extends StatelessWidget {
  Function onPageChanged;
  TaskCardSection(this.onPageChanged(int index));
  @override
  Widget build(BuildContext context) {
    var goalAndTasksList =
        context.watch<GoalAndTaskProvider>().goalAndTasksList;

    if (goalAndTasksList.length > 0) {
      return PageView.builder(
          onPageChanged: (index) => {onPageChanged(index)},
          itemCount: goalAndTasksList.length,
          itemBuilder: (_, i) => TaskCard(goalAndTasksList[i]));
    } else {
      return Center(
          child: Text(
        'No Goal',
        style: Theme.of(context).textTheme.headline4,
      ));
    }
  }
}

// ignore: must_be_immutable
class TaskCard extends StatelessWidget {
  GoalAndTasks goalAndTasks;
  TaskCard(this.goalAndTasks);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: LayoutBuilder(
          builder: (context, constraints) => OpenContainer(
                onClosed: (_) {
                  context.read<GoalAndTaskProvider>().getGoalAndTasksList();
                  context
                      .read<GoalAndTaskProvider>()
                      .checkGoalIsComplete(goalAndTasks.goal.id);
                },
                openElevation: 0,
                closedElevation: 0,
                openColor: Colors.transparent,
                closedColor: Colors.transparent,
                openBuilder: (context, action) => ViewOrEditGoal(),
                closedBuilder: (context, openViewOrEditTask) => InkWell(
                    onTap: () {
                      context
                          .read<GoalAndTaskProvider>()
                          .selectGoal(this.goalAndTasks);
                      openViewOrEditTask();
                    },
                    child: Card(
                        elevation: 1, child: TaskCardDetails(goalAndTasks))),
              )),
    );
  }
}

class NewGoalsButton extends StatefulWidget {
  @override
  _NewGoalsButtonState createState() => _NewGoalsButtonState();
}

class _NewGoalsButtonState extends State<NewGoalsButton> {
  List<GoalAndTasks> goalAndTasksList;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      goalAndTasksList = context.read<GoalAndTaskProvider>().goalAndTasksList;
    });
  }

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      onClosed: (_) {
        context.read<GoalAndTaskProvider>().getGoalAndTasksList();
      },
      openElevation: 0,
      closedElevation: 0,
      openColor: Colors.transparent,
      closedColor: Colors.transparent,
      openBuilder: (context, action) => ViewOrEditGoal(),
      closedBuilder: (context, openViewOrEditTask) => RaisedButton(
        padding: EdgeInsets.only(left: 32, right: 16),
        color: Theme.of(context).primaryColor,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0),
        ),
        child: Row(
          children: [
            Text(
              "NEW GOALS",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            Icon(
              Icons.add,
              color: Colors.white,
            )
          ],
        ),
        onPressed: () async {
          var newGoal = Goal()
            ..title = 'New Goal'
            ..reward = 'Reward'
            ..completed = false;
          await context.read<GoalAndTaskProvider>().createGoal(newGoal);
          openViewOrEditTask();
        },
      ),
    );
  }
}

class Triangle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: ClipPath(
      clipper: CustomTriangleClipper(),
      child: Container(
        width: 25,
        height: 25,
        color: Theme.of(context).cardColor,
      ),
    ));
  }
}

class CustomTriangleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width, size.height / 2);
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class SliderIndicator extends StatelessWidget {
  final bool active;
  SliderIndicator({@required this.active});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 2),
        child: AnimatedContainer(
          duration: Duration(milliseconds: 500),
          height: 4,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: active
                ? Theme.of(context).accentColor
                : Theme.of(context).cardColor,
          ),
        ),
      ),
    );
  }
}
