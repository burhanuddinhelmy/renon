import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:renon/database/database.dart';
import 'package:renon/database/user.dart';
import 'package:renon/provider/user.provider.dart';
import 'package:renon/view/account_setup/account_setup.dart';
import 'package:renon/view/home/home.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await DB().init();

      Future.delayed(Duration(seconds: 2), () async {
        await context.read<UserProvider>().getUser();
        User user = context.read<UserProvider>().user;
        if (user != null) {
          Get.to(Home());
        } else {
          Get.to(AccountSetup());
        }
      });
    });
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "r",
                  style: Theme.of(context).textTheme.headline1.copyWith(
                      color: Theme.of(context).accentColor,
                      fontWeight: FontWeight.w900),
                ),
                Text(
                  "e",
                  style: Theme.of(context).textTheme.headline1.copyWith(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.w900),
                ),
              ],
            ),
            Text(
              "renon",
              style: Theme.of(context)
                  .textTheme
                  .headline6
                  .copyWith(color: Theme.of(context).primaryColor),
            ),
          ],
        ),
      ),
    );
  }
}
