import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:renon/database/user.dart';
import 'package:renon/provider/user.provider.dart';
import 'package:renon/view/account_setup/character_selections.dart';

class AccountSettings extends StatefulWidget {
  @override
  _AccountSettingsState createState() => _AccountSettingsState();
}

class _AccountSettingsState extends State<AccountSettings> {
  int _selectedCharacterId = 0;
  String name;

  @override
  void initState() {
    name = context.read<UserProvider>().user.name;
    _selectedCharacterId = context.read<UserProvider>().user.characterId;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit profile",
            style: TextStyle(
                color: Theme.of(context).textTheme.caption.color,
                fontWeight: FontWeight.bold)),
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme:
            IconThemeData(color: Theme.of(context).textTheme.caption.color),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.all(16)),
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  CharacterSelection(
                      initialCharacterId:
                          context.watch<UserProvider>().user.characterId,
                      onCharacterChange: (selectedCharcterIndex) {
                        _selectedCharacterId = selectedCharcterIndex;
                        return;
                      }),
                  TextField(
                    controller: TextEditingController(
                        text: context.watch<UserProvider>().user.name),
                    onChanged: (text) {
                      name = text;
                    },
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline4,
                    decoration: InputDecoration(
                        hintText: 'Enter your name',
                        alignLabelWithHint: true,
                        hintStyle: TextStyle(color: Colors.black12)),
                  ),
                ],
              )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                    padding: EdgeInsets.only(left: 32, right: 16),
                    color: Theme.of(context).primaryColor,
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    child: Row(
                      children: [
                        Text("SAVE",
                            style: GoogleFonts.montserrat(
                              textStyle: Theme.of(context).textTheme.button,
                              color: Colors.white,
                            )),
                        Icon(
                          Icons.chevron_right,
                          color: Theme.of(context).cardColor,
                        )
                      ],
                    ),
                    onPressed: () async {
                      User user = context.read<UserProvider>().user;
                      user.name = name;
                      user.characterId = _selectedCharacterId;
                      await context.read<UserProvider>().updateUser(user);
                      Get.back();
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
