import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:renon/database/user.dart';
import 'package:renon/provider/user.provider.dart';
import 'package:renon/view/account_setup/character_selections.dart';
import 'package:renon/view/home/home.dart';

class AccountSetup extends StatefulWidget {
  @override
  _AccountSetupState createState() => _AccountSetupState();
}

class _AccountSetupState extends State<AccountSetup> {
  int _selectedCharacterId = 0;
  String name;

  @override
  Widget build(BuildContext context) {
    TextStyle _headline4 = Theme.of(context).textTheme.headline4;

    Widget _renderTitle(text) {
      return Text(text,
          style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                  fontWeight: FontWeight.w700, fontSize: _headline4.fontSize),
              color: Theme.of(context).primaryColor));
    }

    Widget _renderAccentTitle(text) {
      return Text(text,
          style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                  fontWeight: FontWeight.w700, fontSize: _headline4.fontSize),
              color: Theme.of(context).accentColor));
    }

    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.all(16)),
              _renderTitle("Welcome"),
              Row(
                children: [
                  _renderAccentTitle("to"),
                  Padding(padding: EdgeInsets.only(left: 8)),
                  _renderTitle("renon"),
                ],
              ),
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  CharacterSelection(
                      onCharacterChange: (selectedCharcterIndex) {
                    _selectedCharacterId = selectedCharcterIndex;
                    return;
                  }),
                  TextField(
                    onChanged: (text) {
                      name = text;
                    },
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline4,
                    decoration: InputDecoration(
                        hintText: 'Enter your name',
                        alignLabelWithHint: true,
                        hintStyle: TextStyle(color: Colors.black12)),
                  ),
                ],
              )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                    padding: EdgeInsets.only(left: 32, right: 16),
                    color: Theme.of(context).primaryColor,
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    child: Row(
                      children: [
                        Text("CONTINUE",
                            style: GoogleFonts.montserrat(
                              textStyle: Theme.of(context).textTheme.button,
                              color: Colors.white,
                            )),
                        Icon(
                          Icons.chevron_right,
                          color: Theme.of(context).cardColor,
                        )
                      ],
                    ),
                    onPressed: () async {
                      User user = User()
                        ..name = name
                        ..completedGoals = 0
                        ..completedTasks = 0
                        ..characterId = _selectedCharacterId;
                      await context.read<UserProvider>().createUser(user);
                      Get.offAll(Home());
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
