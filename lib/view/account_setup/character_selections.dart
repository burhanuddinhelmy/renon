import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Character {
  int id;
  String name;
  Character({@required this.id, @required this.name});
}

class CharacterSelection extends StatefulWidget {
  Function onCharacterChange;

  int initialCharacterId;
  CharacterSelection(
      {this.onCharacterChange(int _selectedCharacterIndex),
      this.initialCharacterId = 1});
  @override
  _CharacterSelectionState createState() =>
      _CharacterSelectionState(this.initialCharacterId);
}

class _CharacterSelectionState extends State<CharacterSelection> {
  int _selectedCharacterIndex;
  _CharacterSelectionState(initialCharacterId) {
    _selectedCharacterIndex = initialCharacterId;
  }
  List<Character> characters = [
    Character(id: 1, name: "BLU"),
    Character(id: 2, name: "SNOW"),
    Character(id: 3, name: "PINK")
  ];
  Widget _renderCharacter({index}) {
    bool _selected = (_selectedCharacterIndex == index);
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedCharacterIndex = index;
          widget.onCharacterChange(index);
        });
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AnimatedContainer(
            duration: Duration(milliseconds: 200),
            child: Transform.scale(
                scale: 2,
                child: Image(
                    image: AssetImage('assets/image/monster/$index.png'))),
            curve: Curves.easeIn,
            height: 80,
            width: 80,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
              boxShadow: _selected
                  ? [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      )
                    ]
                  : [],
            ),
          ),
          _selected
              ? Padding(
                  padding: EdgeInsets.only(top: 8),
                  child: AnimatedContainer(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                    decoration: BoxDecoration(
                        color: _selected
                            ? Theme.of(context).accentColor
                            : Colors.transparent,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    duration: Duration(milliseconds: 200),
                    child: Text(
                      characters
                          .firstWhere((character) =>
                              character.id == _selectedCharacterIndex)
                          .name,
                      style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontSize:
                              Theme.of(context).textTheme.caption.fontSize),
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      ...characters.map((character) {
        return _renderCharacter(index: character.id);
      }).toList()
    ]);
  }
}
