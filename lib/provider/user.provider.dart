import 'package:flutter/foundation.dart';
import 'package:renon/database/user.dart';

class UserProvider with ChangeNotifier, DiagnosticableTreeMixin {
  User _user = User();
  User get user => _user;

  Future<void> createUser(User user) async {
    await UserDB().insert(user);
    notifyListeners();
  }

  Future<void> getUser() async {
    _user = await UserDB().getUser();
    notifyListeners();
  }

  Future<void> updateUser(User user) async {
    await UserDB().update(user);
  }

  /// Makes `Counter` readable inside the devtools by listing all of its properties

}
