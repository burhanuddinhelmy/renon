import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:renon/database/goal.dart';
import 'package:renon/database/task.dart';
import 'package:renon/database/user.dart';

class GoalAndTaskProvider with ChangeNotifier, DiagnosticableTreeMixin {
  GoalAndTasks _goalAndTasks = GoalAndTasks();
  static final GlobalKey<FormFieldState<String>> _editGoalAndTaskKey =
      GlobalKey<FormFieldState<String>>();
  int _finishGoalCount = 0;
  int _finishTaskCount = 0;
  List<GoalAndTasks> _goalAndTasksList = [];
  GoalAndTasks get goalAndTasks => _goalAndTasks;
  GlobalKey<FormFieldState<String>> get editGoalAndTaskKey =>
      _editGoalAndTaskKey;
  int get finishGoalCount => _finishGoalCount;
  int get finishTaskCount => _finishTaskCount;
  List<GoalAndTasks> get goalAndTasksList => _goalAndTasksList;

  Future<void> createGoal(Goal goal) async {
    await GoalDB().insert(goal);
    _goalAndTasksList = await GoalDB().getGoalAndTasksList();
    _goalAndTasks = _goalAndTasksList.first;
    notifyListeners();
  }

  Future<void> createTask(Task task) async {
    await TaskDB().insert(task);
    _goalAndTasks = await GoalDB().getGoalWithTasks(task.goalId);
    _goalAndTasksList = await GoalDB().getGoalAndTasksList();
    notifyListeners();
  }

  Future<void> updateTask(Task task) async {
    await TaskDB().update(task);
    _goalAndTasksList = await GoalDB().getGoalAndTasksList();
    notifyListeners();
  }

  Future<void> updateGoal(Goal goal) async {
    await GoalDB().update(goal);
    _goalAndTasksList = await GoalDB().getGoalAndTasksList();
    notifyListeners();
  }

  Future<void> deleteTask(Task task) async {
    await TaskDB().delete(task.id);
    _goalAndTasksList = await GoalDB().getGoalAndTasksList();
    notifyListeners();
  }

  updateSelectGoal(int id) async {
    _goalAndTasks = await GoalDB().getGoalWithTasks(id);
    notifyListeners();
  }

  int getNewGoalId() {
    return _goalAndTasksList.last.goal.id;
  }

  selectGoal(GoalAndTasks goalAndTasks) {
    _goalAndTasks = goalAndTasks;
    notifyListeners();
  }

  deleteGoal(GoalAndTasks goalAndTasks) async {
    await GoalDB().delete(goalAndTasks.goal.id);
    await TaskDB().deleteByGoalId(goalAndTasks.goal.id);
    _goalAndTasksList = await GoalDB().getGoalAndTasksList();
  }

  Future<void> getGoalAndTasksList() async {
    _goalAndTasksList = await GoalDB().getGoalAndTasksList();
    notifyListeners();
  }

  Future<void> getFinishedTaskAndGoalCount() async {
    _finishTaskCount = await TaskDB().getCompletedTaskCount();
    _finishGoalCount = await GoalDB().getCompletedGoalCount();
    notifyListeners();
  }

  Future<void> checkGoalIsComplete(int id) async {
    var taskList = await TaskDB().getTaskList(id);
    var goal = await GoalDB().getGoal(id);
    var completedTask = taskList.where((task) => task.completed);
    if (completedTask.length == taskList.length) {
      goal.completed = true;
    } else {
      goal.completed = false;
    }
    await GoalDB().update(goal);
    await getFinishedTaskAndGoalCount();
  }
}
